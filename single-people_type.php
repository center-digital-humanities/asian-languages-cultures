<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article">
						<header id="bio">
							<h1 class="page-title"><?php the_title(); ?></h1>
							<?php if(get_field('photo')) {
								$image = get_field('photo');
								if( !empty($image) ): 
								// vars
								$url = $image['url'];
								$title = $image['title'];
								// thumbnail
								$size = 'people-large';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ];
							endif; ?>
							<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
							<?php } else { ?>
							<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="Silhouette" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
							<?php } ?>
							<div class="details">
								<?php if(get_field('email_address')) { ?>
									<?php $person_email = antispambot(get_field('email_address')); ?>
									<span><strong>E-mail:</strong> <a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a></span>
								<?php } ?>
								<?php if(get_field('phone_number')) { ?>
									<span><strong>Phone: </strong><?php the_field('phone_number'); ?></span>
								<?php } ?>
								<?php if(get_field('office')) { ?>
									<span><strong>Office: </strong><?php the_field('office'); ?></span>
								<?php } ?>
								<?php if(get_field('office_hours')) { ?>
									<p><strong>Office Hours: </strong><?php the_field('office_hours'); ?></p>
								<?php } ?>
							</div>
						</header>
						<section class="bio">
							<?php the_content(); ?>
						</section>
						<?php if(get_field('education')) { ?>
						<section id="education">
							<h2>Education</h2>
							<?php the_field('education'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('research')) { ?>
						<section id="research">
							<h2>Research</h2>
							<?php the_field('research'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('publications')) { ?>
						<section id="publications">
							<h2>Publications</h2>
							<?php the_field('publications'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('articles')) { ?>
						<section id="articles">
							<h2>Articles</h2>
							<?php the_field('articles'); ?>
						</section>
						<?php } ?>
					</article>
					<?php endwhile; ?>
					<?php else : endif; ?>
				</div>
				<div class="col">
					<?php if(get_field('photo')) {
						$image = get_field('photo');
						if( !empty($image) ): 
						// vars
						$url = $image['url'];
						$title = $image['title'];
						// thumbnail
						$size = 'bones-thumb-300';
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ];
					endif; ?>
					<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
					<?php } else { ?>
					<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="Silhouette" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
					<?php } ?>						
					<div class="content col-nav">
						<nav role="navigation" aria-label="Person Navigation">
							<?php 
							// Make nav appear if only if there is anything to show
							if(get_field('education') || get_field('research') || get_field('books') || get_field('articles') || get_field('dissertations') || get_field('courses') || get_field('custom_section_title')) { ?>
								<ul class="table-of-contents">
									<h3>Table of Contents</h3>
									<?php if( empty( $post->post_content) ) {
									// If there is no bio, don't show bio link
									} else { ?>
									<li><a href="#bio">Bio</a></li>
									<?php } ?>
									<?php if(get_field('education')) { ?>
									<li><a href="#education">Education</a></li>
									<?php } ?>
									<?php if(get_field('research')) { ?>
									<li><a href="#research">Research</a></li>
									<?php } ?>
									<?php if(get_field('books')) { ?>
									<li><a href="#books">Books</a></li>
									<?php } ?>
									<?php if(get_field('articles')) { ?>
									<li><a href="#articles">Articles</a></li>
									<?php } ?>
									<?php if(get_field('dissertations')) { ?>
									<li><a href="#dissertations">Dissertation</a></li>
									<?php } ?>
									<?php if(get_field('courses')) { ?>
									<li><a href="#courses">Courses</a></li>
									<?php } ?>
									<?php if(get_field('custom_section_title')) { ?>
									<li><a href="#other"><?php the_field('custom_section_title'); ?></a></li>
									<?php } ?>
								</ul>
							<?php }
							if(get_field('cv') || get_field('personal_website') || get_field('academia_profile') || get_field('additional_link')) { ?>
								<ul class="additional-links">
									<h3>Additional Links</h3>
									<?php if(get_field('cv')) { ?>
									<li class="download"><span class="fas fa-download"></span><a href="<?php the_field('cv'); ?>">Download CV</a></li>
									<?php } ?>
									<?php if(get_field('personal_website')) { ?>
									<li class="link"><a href="<?php the_field('personal_website'); ?>"><span class="fas fa-link"></span>Personal Website</a></li>
									<?php } ?>
									<?php if(get_field('academia_profile')) { ?>
									<li class="link"><a href="<?php the_field('academia_profile'); ?>"><span class="fas fa-link"></span>Academia Profile</a></li>
									<?php } ?>
									<?php if(get_field('additional_link')) { ?>
									<li class="link"><a href="<?php the_field('additional_link'); ?>"><span class="fas fa-link"></span><?php the_field('additional_link_title'); ?></a></li>
									<?php } ?>
								</ul>
							<?php } ?>
						</nav>
					</div>
				</div>	
			</div>
<?php get_footer(); ?>