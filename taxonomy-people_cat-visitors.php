<?php get_header(); ?>

			<div class="content main">
			
				<header>
					<h1><?php single_cat_title(); ?></h1>
					<?php $category_description = category_description();
					if ( ! empty( $category_description ) )
					echo apply_filters( 'category_archive_meta', '<p>' . $category_description . '</p>' );
					?>
				</header>			
				<?php 						
					$current_visitors = new WP_Query( array( 'people_cat' => 'visitors', 'post_type' => 'people_type', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'visitor_status', 'value' => 'current'))
					));
					
					$past_visitors = new WP_Query( array( 'people_cat' => 'visitors', 'post_type' => 'people_type', 'posts_per_page' => -1,  'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'visitor_status', 'value' => 'past'))
					));
				?>
				<?php if ( $past_visitors->have_posts() ) { ?>
				<h2>Current Visitors</h2>
				<?php } ?>
				<div class="people-list">
					<ul <?php post_class('cf'); ?>>	
					<?php while ( $current_visitors->have_posts() ) : $current_visitors->the_post(); ?>
										
						<a href="<?php the_permalink() ?>" class="person-item <?php if(get_field('field')) { echo implode(' ', get_field('field')); } ?>">
							<li><?php if(get_field('photo')) {
								$image = get_field('photo');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'bones-thumb-100';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo" />
								<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" width="100px" height="100px" class="photo" />
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<?php if(get_field('position_title')) { ?>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php } ?>
									<?php if(get_field('visiting_duration')) { ?>
									<dd class="interest"><?php the_field('visiting_duration'); ?></dd>
									<?php } ?>
								</dl>
							</li>
						</a>

					<?php endwhile; ?>
					
					</ul>
				</div>
				<?php if ( $past_visitors->have_posts() ) { ?>
				<h2>Past Visitors</h2>
				<div class="people-list">
					<ul <?php post_class('cf'); ?>>
					<?php while ( $past_visitors->have_posts() ) : $past_visitors->the_post(); ?>
										
						<a href="<?php the_permalink() ?>" class="person-item <?php if(get_field('field')) { echo implode(' ', get_field('field')); } ?>">
							<li><?php if(get_field('photo')) {
								$image = get_field('photo');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'bones-thumb-100';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo" />
								<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" width="100px" height="100px" class="photo" />
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<?php if(get_field('position_title')) { ?>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php } ?>
									<?php if(get_field('visiting_duration')) { ?>
									<dd class="interest"><?php the_field('visiting_duration'); ?></dd>
									<?php } ?>
								</dl>
							</li>
						</a>

					<?php endwhile; ?>
					
					</ul>

				</div>
				<?php } ?>

			</div>

<?php get_footer(); ?>
