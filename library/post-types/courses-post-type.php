<?php
// Courses Post Type Settings

// let's create the function for the custom type
function courses_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'courses_type', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Courses', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Course', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Courses', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Course', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Course', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Course', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Course', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Courses', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No courses added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all department courses', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 7, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-welcome-learn-more', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'courses', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'courses_type', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt')
		) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type( 'category', 'courses_type' );
	/* this adds your post tags to your custom post type */
	register_taxonomy_for_object_type( 'post_tag', 'courses_type' );
	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'courses_post_type');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
	register_taxonomy( 'courses_cat', 
		array('courses_type'), /* if you change the name of register_post_type( 'courses_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Courses Categories', 'bonestheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Course Category', 'bonestheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Courses Categories', 'bonestheme' ), /* search title for taxomony */
				'all_items' => __( 'All Courses Categories', 'bonestheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Courses Category', 'bonestheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Courses Category:', 'bonestheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Course Category', 'bonestheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Course Category', 'bonestheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Course Category', 'bonestheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Course Category Name', 'bonestheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'custom-slug' ),
		)
	);
	
	// now let's add custom tags (these act like categories)
	register_taxonomy( 'courses_tag', 
		array('courses_type'), /* if you change the name of register_post_type( 'courses_type', then you have to change this */
		array('hierarchical' => false,    /* if this is false, it acts like tags */
			'labels' => array(
				'name' => __( 'Courses Tags', 'bonestheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Course Tag', 'bonestheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Course Tags', 'bonestheme' ), /* search title for taxomony */
				'all_items' => __( 'All Course Tags', 'bonestheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Course Tag', 'bonestheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Course Tag:', 'bonestheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Course Tag', 'bonestheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Course Tag', 'bonestheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Course Tag', 'bonestheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Course Tag Name', 'bonestheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
		)
	);
?>
