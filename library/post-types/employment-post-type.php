<?php
// Courses Post Type Settings

// let's create the function for the custom type
function employment_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'employment_type', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Job Openings', 'bonestheme' ), /* This is the Title of the Research */
			'singular_name' => __( 'Job Opening', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Job Openings', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Job Opening', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Job Opening', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Job Opening', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Job Opening', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Job Openings', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No job opening added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all job openings', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 9, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-portfolio', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'position', 'with_front' => false ), /* you can specify its url slug */
			//'has_archive' => 'research', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt')
		) /* end of options */
	); /* end of register post type */	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'employment_post_type');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
	register_taxonomy( 'employment_cat', 
		array('employment_type'), /* if you change the name of register_post_type( 'books_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Job Categories', 'bonestheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Job Category', 'bonestheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Job Categories', 'bonestheme' ), /* search title for taxomony */
				'all_items' => __( 'All Job Categories', 'bonestheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Job Category', 'bonestheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Job Category:', 'bonestheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Job Category', 'bonestheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Job Category', 'bonestheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Job Category', 'bonestheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Job Category Name', 'bonestheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'positions' ),
		)
	);
	
	// now let's add custom tags (these act like categories)
	register_taxonomy( 'employment_tag', 
		array('employment_type'), /* if you change the name of register_post_type( 'books_type', then you have to change this */
		array('hierarchical' => false,    /* if this is false, it acts like tags */
			'labels' => array(
				'name' => __( 'Job Tags', 'bonestheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Job Tag', 'bonestheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Job Tags', 'bonestheme' ), /* search title for taxomony */
				'all_items' => __( 'All Job Tags', 'bonestheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Job Tag', 'bonestheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Job Tag:', 'bonestheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Job Tag', 'bonestheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Job Tag', 'bonestheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Job Tag', 'bonestheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Job Tag Name', 'bonestheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
		)
	);
?>
