<?php
// Courses Post Type Settings

// let's create the function for the custom type
function research_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'research_type', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Research', 'bonestheme' ), /* This is the Title of the Research */
			'singular_name' => __( 'Research', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Research', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Research', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Research', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Research', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Research', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Research', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No books added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all books', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 9, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-portfolio', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'project', 'with_front' => false ), /* you can specify its url slug */
			//'has_archive' => 'research', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt')
		) /* end of options */
	); /* end of register post type */	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'research_post_type');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
	register_taxonomy( 'research_cat', 
		array('research_type'), /* if you change the name of register_post_type( 'books_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Research Categories', 'bonestheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Research Category', 'bonestheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Research Categories', 'bonestheme' ), /* search title for taxomony */
				'all_items' => __( 'All Research Categories', 'bonestheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Research Category', 'bonestheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Research Category:', 'bonestheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Research Category', 'bonestheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Research Category', 'bonestheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Research Category', 'bonestheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Research Category Name', 'bonestheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'research-topics' ),
		)
	);
	
	// now let's add custom tags (these act like categories)
	register_taxonomy( 'research_tag', 
		array('research_type'), /* if you change the name of register_post_type( 'books_type', then you have to change this */
		array('hierarchical' => false,    /* if this is false, it acts like tags */
			'labels' => array(
				'name' => __( 'Research Tags', 'bonestheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Research Tag', 'bonestheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Research Tags', 'bonestheme' ), /* search title for taxomony */
				'all_items' => __( 'All Research Tags', 'bonestheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Research Tag', 'bonestheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Research Tag:', 'bonestheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Research Tag', 'bonestheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Research Tag', 'bonestheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Research Tag', 'bonestheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Research Tag Name', 'bonestheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
		)
	);
?>
