<?php
// Courses Post Type Settings

// let's create the function for the custom type
function books_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'books_type', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Books', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Book', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Books', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Book', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Book', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Book', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Book', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Books', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No books added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all books', 'bonestheme' ), /* Custom Type Description */
			'public' => true, // Change to true later
			'publicly_queryable' => true, // Change to true later
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-book-alt', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'book', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'books', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt')
		) /* end of options */
	); /* end of register post type */	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'books_post_type');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
	register_taxonomy( 'books_cat', 
		array('books_type'), /* if you change the name of register_post_type( 'books_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Books Categories', 'bonestheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Books Category', 'bonestheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Book Categories', 'bonestheme' ), /* search title for taxomony */
				'all_items' => __( 'All Book Categories', 'bonestheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Book Category', 'bonestheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Book Category:', 'bonestheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Book Category', 'bonestheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Book Category', 'bonestheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Book Category', 'bonestheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Book Category Name', 'bonestheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'books' ),
		)
	);
?>
