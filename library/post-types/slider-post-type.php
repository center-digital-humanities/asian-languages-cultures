<?php
// Slides Post Type Settings

// let's create the function for the custom type
function slider_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'slider_type', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Slides', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Slide', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Slides', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Slide', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Slide', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Slide', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Slide', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Slides', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No slided added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all slides', 'bonestheme' ), /* Custom Type Description */
			'public' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 7, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-media-code', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'slide', 'with_front' => false ), /* you can specify its url slug */
			 /*'has_archive' => 'slider_type', you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt')
		) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
		register_taxonomy_for_object_type( 'category', 'slider_type' );
		/* this adds your post tags to your custom post type */
		register_taxonomy_for_object_type( 'post_tag', 'slider_type' );
		
}
	
		// adding the function to the Wordpress init
		add_action( 'init', 'slider_post_type');
		
		/*
		for more information on taxonomies, go here:
		http://codex.wordpress.org/Function_Reference/register_taxonomy
		*/
		
		// now let's add custom categories (these act like categories)
		register_taxonomy( 'slider_cat', 
			array('slider_type'), /* if you change the name of register_post_type( 'slider_type', then you have to change this */
			array('hierarchical' => true,     /* if this is true, it acts like categories */
				'labels' => array(
					'name' => __( 'Slides Categories', 'bonestheme' ), /* name of the custom taxonomy */
					'singular_name' => __( 'Slide Category', 'bonestheme' ), /* single taxonomy name */
					'search_items' =>  __( 'Search Slides Categories', 'bonestheme' ), /* search title for taxomony */
					'all_items' => __( 'All Slides Categories', 'bonestheme' ), /* all title for taxonomies */
					'parent_item' => __( 'Parent Slides Category', 'bonestheme' ), /* parent title for taxonomy */
					'parent_item_colon' => __( 'Parent Slides Category:', 'bonestheme' ), /* parent taxonomy title */
					'edit_item' => __( 'Edit Slide Category', 'bonestheme' ), /* edit custom taxonomy title */
					'update_item' => __( 'Update Slide Category', 'bonestheme' ), /* update title for taxonomy */
					'add_new_item' => __( 'Add New Slide Category', 'bonestheme' ), /* add new title for taxonomy */
					'new_item_name' => __( 'New Slide Category Name', 'bonestheme' ) /* name title for taxonomy */
				),
				'show_admin_column' => true, 
				'show_ui' => true,
				'query_var' => true,
				'rewrite' => array( 'slug' => 'custom-slug' ),
			)
		);
	
	?>
