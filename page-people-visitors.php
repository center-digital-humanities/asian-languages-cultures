<?php
/*
 Template Name: Visitors Listing
*/
?>
<?php get_header(); ?>

			<div class="content main" id="main-content">
				<header>
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
					<?php // Select what people category to show
					$people_category = get_field('people_category');
					if( $people_category ) {
						$people_cat = $people_category->slug;
					}
					// Set varaibles to decide behavior of page
					if ( get_field('link_to_pages') == 'yes' ) {
						$person_link = 'yes';
					}
					$people_details = get_field('people_details');
					if( in_array('position', $people_details) ) { 
						$position = 'yes';
					} 
					if( in_array('interest', $people_details) ) {
						$interest = 'yes';
					} 
					if( in_array('email', $people_details) ) {
						$email = 'yes';
					}
					if( in_array('phone', $people_details) ) {
						$phone = 'yes';
					}
					if( in_array('duration', $people_details) ) {
						$duration = 'yes';
					} 
					?>
				</header>
				<?php 						
					$current_visitors = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people_type', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'visitor_status', 'value' => 'current'))));
					
					$past_visitors = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people_type', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'visitor_status', 'value' => 'past'))));
				?>
				<?php if ( $past_visitors->have_posts() ) { ?>
				<h2>Current Visitors</h2>
				<?php } ?>
				<div class="people-list">
					<ul class="<?php echo $people_cat ?>">
					<?php while ( $current_visitors->have_posts() ) : $current_visitors->the_post(); ?>
						<?php if ( $person_link == 'yes' ) { ?>
						<a href="<?php the_permalink() ?>" class="person-item <?php if(get_field('field')) { echo implode(' ', get_field('field')); } ?> <?php the_field('person_type'); ?> <?php if ( $person_link == 'yes' ) { ?>hover<?php } ?>">
						<?php } ?>
							<li <?php if (empty($person_link)) { ?> class="person-item <?php if(get_field('field')) { echo implode(' ', get_field('field')); } ?> <?php the_field('person_type'); ?><?php } ?>">
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'bones-thumb-100';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<?php // If this pages uses email and links to pages
								if ( $person_link == 'yes' && $email == 'yes' ) { ?>
								</a>
								<?php } ?>
								<dl>
									<dt class="name">
										<?php // If this pages uses email and links to pages
										if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										<a href="<?php the_permalink() ?>">
										<?php } ?><?php the_title(); ?><?php if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										</a>
										<?php } ?>
									</dt>
									<?php if ( $email == 'yes' ) { 
									if(get_field('email_address')) {
										$person_email = antispambot(get_field('email_address')); ?>
									<dd class="email">
										<a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a>
									</dd>
									<?php }
									}
									if ( $phone == 'yes' ) { 
									if(get_field('phone_number')) { ?>
									<dd class="phone"><?php the_field('phone_number'); ?></dd>
									<?php } 
									}
									if ( $position == 'yes' ) {
									if(get_field('position_title')) { ?>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php }
									}
									if ( $interest == 'yes' ) {
									if(get_field('interest')) { ?>
									<dd class="interest"><?php the_field('interest'); ?></dd>
									<?php }
									} 
									if ( $duration == 'yes' ) {
									if(get_field('visiting_duration')) { ?>
									<dd class="duration"><?php the_field('visiting_duration'); ?></dd>
									<?php }
									}
									?>
								</dl>
							</li>
						<?php if ( $person_link == 'yes' ) { ?>
						</a>
						<?php } ?>
					<?php endwhile; ?>
					</ul>
				</div>
				<?php if ( $past_visitors->have_posts() ) { ?>
				<h2>Past Visitors</h2>
				<div class="people-list">
					<ul class="<?php echo $people_cat ?>">
					<?php while ( $past_visitors->have_posts() ) : $past_visitors->the_post(); ?>
						<?php if ( $person_link == 'yes' ) { ?>
						<a href="<?php the_permalink() ?>" class="person-item <?php if(get_field('field')) { echo implode(' ', get_field('field')); } ?> <?php the_field('person_type'); ?> <?php if ( $person_link == 'yes' ) { ?>hover<?php } ?>">
						<?php } ?>
							<li <?php if (empty($person_link)) { ?> class="person-item <?php if(get_field('field')) { echo implode(' ', get_field('field')); } ?> <?php the_field('person_type'); ?><?php } ?>">
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'bones-thumb-100';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<?php // If this pages uses email and links to pages
								if ( $person_link == 'yes' && $email == 'yes' ) { ?>
								</a>
								<?php } ?>
								<dl>
									<dt class="name">
										<?php // If this pages uses email and links to pages
										if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										<a href="<?php the_permalink() ?>">
										<?php } ?><?php the_title(); ?><?php if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										</a>
										<?php } ?>
									</dt>
									<?php if ( $email == 'yes' ) { 
									if(get_field('email_address')) {
										$person_email = antispambot(get_field('email_address')); ?>
									<dd class="email">
										<a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a>
									</dd>
									<?php }
									}
									if ( $phone == 'yes' ) { 
									if(get_field('phone_number')) { ?>
									<dd class="phone"><?php the_field('phone_number'); ?></dd>
									<?php } 
									}
									if ( $position == 'yes' ) {
									if(get_field('position_title')) { ?>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php }
									}
									if ( $interest == 'yes' ) {
									if(get_field('interest')) { ?>
									<dd class="interest"><?php the_field('interest'); ?></dd>
									<?php }
									} 
									if ( $duration == 'yes' ) {
									if(get_field('visiting_duration')) { ?>
									<dd class="duration"><?php the_field('visiting_duration'); ?></dd>
									<?php }
									}
									?>
								</dl>
							</li>
						<?php if ( $person_link == 'yes' ) { ?>
						</a>
						<?php } ?>
					<?php endwhile; ?>
					</ul>
					
				</div>
				<?php } ?>

			</div>

<?php get_footer(); ?>
