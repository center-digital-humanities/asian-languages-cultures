<?php
/*
 Template Name: Placement Exams
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						<h1 class="page-title"><?php the_title(); ?></h1>

						<section>
							<?php the_content(); ?>
							<?php if(get_field('placement_exam_options') == "version_1")
							{ the_field('version_1');
							
							} if(get_field('placement_exam_options') == "version_2")
							{ the_field('version_2');
							
							} if(get_field('placement_exam_options') == "version_3")
							{ the_field('version_3');
							
							} if(get_field('placement_exam_options') == "version_4")
							{ the_field('version_4');
							} ?>
														
							<?php if(get_field('placement_exam_date')) { ?>
							<div class="exam-date">
								<h2>Next exam is <strong><?php the_field('date_in_text'); ?></strong></h2>
								<div id="countdown"></div>
								<script type="text/javascript">
								jQuery("document").ready(function($){
									$('#countdown').countdown('<?php the_field('placement_exam_date'); ?>').on('update.countdown', function(event) {
									   var $this = $(this).html(event.strftime(''
									     + '<strong>%-w</strong> <span>Week%!w</span> '
									     + '<strong>%-d</strong> <span>Day%!d</span> '
										));
									});
								});
								</script>
								<?php if(get_field('registration_link')) { ?>
									<a class="btn" href="<?php the_field('registration_link'); ?>">Register Now</a>
								<?php } ?>
							</div>
							<?php } ?>
							
						</section>

					</article>

					<?php endwhile; else : ?>

					<article id="post-not-found" class="hentry cf">
							<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						<section class="entry-content">
							<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
					</article>

					<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>
