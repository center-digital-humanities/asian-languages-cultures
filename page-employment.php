<?php
/*
 Template Name: Employment
*/
?>

<?php get_header(); ?>
			<div class="content">
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<h1 class="page-title"><?php the_title(); ?></h1>
					<section>
						<?php the_content(); ?>
					</section>
					<?php endwhile; else : ?>
					<?php endif; ?>

					<?php $employment_loop = new WP_Query( array( 'post_type' => 'employment_type', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'desc' ) ); ?>
					<section>
						<?php while ( $employment_loop->have_posts() ) : $employment_loop->the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">	
							<h2 class="entry-title"><?php the_title(); ?></h2>
							<?php the_content(); ?>
							<?php if(get_field('how_to_apply')) { ?>
							<h3>How to Apply</h3>
							<?php the_field('how_to_apply'); ?>
							<?php } ?>
						</article>
						<?php endwhile; ?>
					</section>							
				</div>

				<div class="col">
					<div class="content">
						<nav class="page-nav">
							<?php rewind_posts(); ?>
							<h3>Available Positions</h3>
							<ul>
								<?php while ( $employment_loop->have_posts() ) : $employment_loop->the_post(); ?>
								<li><a href="#post-<?php the_ID(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
								<?php endwhile; ?>
							</ul>
						</nav>
					</div>
				</div>

			</div>

<?php get_footer(); ?>
