<?php get_header(); ?>

			<div class="content main">
			
				<header>
					<h1><?php single_cat_title(); ?></h1>
					<?php $category_description = category_description();
					if ( ! empty( $category_description ) )
					echo apply_filters( 'category_archive_meta', '<p>' . $category_description . '</p>' );
					?>
				</header>

				<div class="people-list">

					<ul <?php post_class('cf'); ?>>
					
					<?php $people_loop = new WP_Query( array( 'people_cat' => 'staff', 'post_type' => 'people_type', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC') ); ?>
					
					<?php while ( $people_loop->have_posts() ) : $people_loop->the_post(); ?>

						<li><?php if(get_field('photo')) {
							$image = get_field('photo');
							if( !empty($image) ): 
								// vars
								$url = $image['url'];
								$title = $image['title'];
								// thumbnail
								$size = 'bones-thumb-100';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ];
							endif; ?>
							<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo" />
							<?php } else { ?>
							<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" width="100px" height="100px" class="photo" />
							<?php } ?>
							<dl>
								<dt class="name"><?php the_title(); ?></dt>
								<?php if(get_field('email_address')) { ?>
								<dd class="email">
									<a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a>
								</dd>
								<?php } ?>									
								<dd class="position"><?php the_field('position_title'); ?></dd>
							</dl>
						</li>

					<?php endwhile; ?>
					</ul>

				</div>

			</div>

<?php get_footer(); ?>