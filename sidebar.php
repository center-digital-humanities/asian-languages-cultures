<?php // For Event month or list landing page, https://gist.github.com/jo-snips/2415009
// Only run if The Events Calendar is installed 
if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || tribe_is_month() && !is_tax()) { 
	// Do nothing	
}

// For posts
elseif (is_single() || is_category() || is_search() || is_tax() || is_tag()) {

	// If People section
	if (is_singular('profile') || is_tax('graduate_year') || is_tax('profile_cat') || is_post_type_archive('profile')) { ?>
	<div class="col side">
		<div class="content">
			<nav class="page-nav" role="navigation" aria-label="Section Navigation">
				<?php wp_nav_menu(array(
					'container' => false,
					'menu' => __( 'Profiles', 'bonestheme' ),
					'menu_class' => 'profile-nav',
					'theme_location' => 'profile-nav',
					'before' => '',
					'after' => '',
					'depth' => 2,
					'items_wrap' => '<h3>Profiles</h3> <ul>%3$s</ul>'
				)); ?>
			</nav>
		</div>
	</div>
	
	<?php } elseif (is_category( 'undergraduate-announcements' ) || in_category('undergraduate-announcements') || is_tag()) { ?> 
		<?php wp_reset_postdata(); ?>
		<div class="col side">
			<div class="content">
				<div class="tags">
					<h3>Tags</h3>
					<?php wp_tag_cloud( 'smallest=1&largest=1&unit=em&number=50&orderby=count&separator=, ' ); ?>
				</div>
				<div class="announcements">
					<h3>Announcements</h3>
					<?php query_posts('category_name=undergraduate-announcements&showposts=5'); ?>
					<ul>
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<li>
								<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><h4><?php the_title(); ?></h4></a>
							</li>
						<?php endwhile; ?>
					</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php } else { ?>
		<div class="col side feed" role="complementary">
			<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
			<?php dynamic_sidebar( 'news-sidebar' ); ?>
			<?php else : endif; ?>
			<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
			<?php dynamic_sidebar( 'events-sidebar' ); ?>
			<?php else : endif; ?>
		</div>
	<?php } 
} ?>
<?php 
// If a Graduate subpage or Group page
if (is_tree(1257) or get_field('menu_select') == "graduate") { ?>
	<div class="col side">
		<div class="content">
			<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
				<?php wp_nav_menu(array(
					'container' => false,
					'menu' => __( 'Graduate', 'bonestheme' ),
					'menu_class' => 'graduate-nav',
					'theme_location' => 'graduate-nav',
					'before' => '',
					'after' => '',
					'depth' => 2,
					'items_wrap' => '<h3>Graduate</h3> <ul>%3$s</ul>'
				)); ?>
			</nav>
		</div>
	</div>
	<div class="col side">
		<div class="content">
			<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
				<?php wp_nav_menu(array(
					'container' => false,
					'menu' => __( 'Graduate Quick Links', 'bonestheme' ),
					'menu_class' => 'graduate-quick-links',
					'theme_location' => 'graduate-quick-links',
					'before' => '',
					'after' => '',
					'depth' => 2,
					'items_wrap' => '<h3>Quick Links</h3> <ul>%3$s</ul>'
				)); ?>
			</nav>
			<div class="grad-news">
				<h3>Graduate News</h3>
				<?php query_posts('category_name=graduate-news&showposts=3'); ?>
				<ul>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<li>
							<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php } ?>
<?php // For pages
if (is_page() || is_404()) { ?>
<div class="col side">
	<div class="content">
		<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
		<?php 
			// If People section
			if (is_page_template( 'page-profiles.php') ) {
				wp_nav_menu(array(
					'container' => false,
					'menu' => __( 'Profiles', 'bonestheme' ),
					'menu_class' => 'profile-nav',
					'theme_location' => 'profile-nav',
					'before' => '',
					'after' => '',
					'depth' => 2,
					'items_wrap' => '<h3>Profiles</h3> <ul>%3$s</ul>'
				));
			}		
			
			// If an Undergraduate subpage
			if (is_tree(1253) or get_field('menu_select') == "undergraduate") {
				wp_nav_menu(array(
				   	'container' => false,
				   	'menu' => __( 'Undergraduate', 'bonestheme' ),
				   	'menu_class' => 'undergrad-nav',
				   	'theme_location' => 'undergrad-nav',
				   	'before' => '',
				   	'after' => '',
				   	'depth' => 2,
				   	'items_wrap' => '<h3>Undergraduate</h3> <ul>%3$s</ul>'
				));
			}
			
			// If Placement Exams page
			if (is_tree(1341)) {
				wp_nav_menu(array(
				   	'container' => false,
				   	'menu' => __( 'Languages', 'bonestheme' ),
				   	'menu_class' => 'languages-nav',
				   	'theme_location' => 'languages-nav',
				   	'before' => '',
				   	'after' => '',
				   	'depth' => 2,
				   	'items_wrap' => '<h3>Languages</h3> <ul>%3$s</ul>'
				));
			}
			
			// For general pages, Resources, About pages Friends of ALC, Search, and 404's
			if (is_tree(1259) or is_tree(1261) or is_tree(1251) or is_search() or is_404() or get_field('menu_select') == "general") {
				wp_nav_menu(array(
					'container' => false,
					'menu' => __( 'Main Menu', 'bonestheme' ),
					'menu_class' => 'side-nav',
					'theme_location' => 'main-nav',
					'before' => '',
					'after' => '',
					'depth' => 1,
					'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
				));
			}
		?>
		</nav>
		<?php if ( is_page( 'give' )  ) : ?>
		<?php dynamic_sidebar( 'give-sidebar' ); ?>
		<?php else : ?>
		<?php endif; ?>
	</div>
</div>
<?php } ?>