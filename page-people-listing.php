<?php
/*
 Template Name: People Listing
*/
?>
<?php get_header(); ?>
			<div class="content main" id="main-content">
				<header>
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
					<?php // Select what people category to show
					$people_category = get_field('people_category');
					if( $people_category ) {
						$people_cat = $people_category->slug;
					}
					// Set varaibles to decide behavior of page
					if ( get_field('link_to_pages') == 'yes' ) {
						$person_link = 'yes';
					}
					$people_details = get_field('people_details');
					if( in_array('position', $people_details) ) { 
						$position = 'yes';
					} 
					if( in_array('interest', $people_details) ) {
						$interest = 'yes';
					} 
					if( in_array('email', $people_details) ) {
						$email = 'yes';
					}
					if( in_array('phone', $people_details) ) {
						$phone = 'yes';
					} 
					?>
					<?php if ( get_field('display_field_of_study') == 'show' ) { ?>
					<?php if ( $people_cat == 'faculty' ) { 
					if ( has_nav_menu('faculty-filter') ) { ?> 
					<div class="filter">
						<div class="options button-group" data-filter-group="field">
							<div class="general-filters">	
								<button data-filter="" data-text="View All" class="option all is-checked">View All</button>
								<button data-filter=".officers" data-text="Department Officers" class="option officers">Department Officers</button>
							</div>
							<?php // To make another filter, duplicate the div below ?>
							<div class="field-filters">
							<?php if(get_field('filter_label')) { ?>
								<h3><?php the_field('filter_label'); ?></h3>
							<?php } ?>
								<?php wp_nav_menu(array(
									'container' => false,
									'menu' => __( 'Faculty Filter', 'bonestheme' ),
									'menu_class' => 'faculty-filter',
									'theme_location' => 'faculty-filter',
									'before' => '',
									'after' => '',
									'depth' => 1,
									'items_wrap' => '%3$s',
									'walker' => new Filter_Walker
								)); ?>
							</div>
						</div>
					</div>
					<h2 class="filter-title">All</h2>
					<?php }
					}?>
					<?php if ( $people_cat == 'grad' ) { 
					if ( has_nav_menu('faculty-filter') ) { ?> 
					<div class="filter">
						<div class="options button-group" data-filter-group="field">
							<?php // To make another filter, duplicate the div below ?>
							<div>
							<?php if(get_field('filter_label')) { ?>
								<h3><?php the_field('filter_label'); ?></h3>
							<?php } ?>
								<button data-filter="" data-text="View All" class="option all is-checked">View All</button>
								<?php wp_nav_menu(array(
									'container' => false,
									'menu' => __( 'Grad Filter', 'bonestheme' ),
									'menu_class' => 'grad-filter',
									'theme_location' => 'grad-filter',
									'before' => '',
									'after' => '',
									'depth' => 1,
									'items_wrap' => '%3$s',
									'walker' => new Filter_Walker
								)); ?>
							</div>
						</div>
					</div>
					<h2 class="filter-title">All</h2>
					<?php }
					}?>
					<?php } ?>
				</header>
				<div class="people-list">
					<ul class="<?php echo $people_cat ?>">
					<?php $core_loop = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people_type', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC')); ?>
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
						
						<li class="person-item <?php if(get_field('field')) { echo implode(' ', get_field('field')); } ?> <?php the_field('person_type'); ?> <?php if ( $person_link == 'yes' ) { ?>hover<?php } ?>">
						
							<?php if ( $person_link == 'yes' ) { ?>
							<a href="<?php the_permalink() ?>">
							<?php } ?>
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'bones-thumb-100';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<?php // If this pages uses email and links to pages
								if ( $person_link == 'yes' && $email == 'yes' ) { ?>
								</a>
								<?php } ?>
								<dl>
									<dt class="name">
										<?php // If this pages uses email and links to pages
										if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										<a href="<?php the_permalink() ?>">
										<?php } ?><?php the_title(); ?><?php if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										</a>
										<?php } ?>
									</dt>
									<?php if ( $email == 'yes' ) { 
									if(get_field('email_address')) {
										$person_email = antispambot(get_field('email_address')); ?>
									<dd class="email">
										<a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a>
									</dd>
									<?php }
									}
									if ( $phone == 'yes' ) { 
									if(get_field('phone_number')) { ?>
									<dd class="phone"><?php the_field('phone_number'); ?></dd>
									<?php } 
									}
									if ( $position == 'yes' ) {
									if(get_field('position_title')) { ?>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php }
									}
									if ( $interest == 'yes' ) {
									if(get_field('interest')) { ?>
									<dd class="interest"><?php the_field('interest'); ?></dd>
									<?php }
									} ?>
								</dl>
						<?php if ( $person_link == 'yes' ) { ?>
							</a>
						<?php } ?>
						</li>
					<?php endwhile; ?>					
					</ul>					
				</div>
			</div>
<?php get_footer(); ?>