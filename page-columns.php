<?php
/*
 Template Name: Columns Template
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						<h1 class="page-title"><?php the_title(); ?></h1>

						<section class="full">
							<?php the_content(); ?>
						</section>
						<section class="col-container">
							<div class="left-col">
								<?php the_field('left_column'); ?>
							</div>
							<div class="right-col">
								<?php the_field('right_column'); ?>
							</div>
						</section>

					</article>

					<?php endwhile; else : ?>

					<article id="post-not-found" class="hentry cf">
							<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						<section class="entry-content">
							<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
					</article>

					<?php endif; ?>

				</div>
			</div>

<?php get_footer(); ?>
