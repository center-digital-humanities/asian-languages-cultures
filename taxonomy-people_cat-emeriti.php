<?php get_header(); ?>

			<div class="content main">
			
				<header>
					<h1>Emeriti</h1>
					<?php $category_description = category_description();
					if ( ! empty( $category_description ) )
					echo apply_filters( 'category_archive_meta', '<p>' . $category_description . '</p>' );
					?>
				</header>			

				<div class="people-list">

					<ul <?php post_class('cf'); ?>>
					
					<?php 						
						$core_loop = new WP_Query( array( 'people_cat' => 'emeriti', 'post_type' => 'people_type', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC'));						
					?>
					
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
										
						<li class="person-item <?php if(get_field('field')) { echo implode(' ', get_field('field')); } ?>">
							<dl>
								<a href="<?php the_permalink() ?>"><dt class="name"><?php the_title(); ?></dt></a>
								<?php if(get_field('position_title')) { ?>
								<dd class="position"><?php the_field('position_title'); ?></dd>
								<?php } ?>
								<?php if(get_field('email_address')) { ?>
								<dd class="email">
									<a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a>
								</dd>
								<?php } ?>
							</dl>							
						</li>
						

					<?php endwhile; ?>
					
					</ul>

				</div>

			</div>

<?php get_footer(); ?>
