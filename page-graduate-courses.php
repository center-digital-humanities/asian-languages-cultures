<?php
/*
 Template Name: Graduate Courses
*/
?>

<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php 
						$term = get_field('quarter');
						$qt = $term->name;
					?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						<h1 class="page-title"><?php the_title(); ?></h1>

						<section>
							<?php the_content(); ?>
						</section>

					</article>
					
					<?php // Spanish Courses ?>
					<?php $spanish_courses_loop = new WP_Query( 
						array( 'courses_cat' => "'$qt'", 'post_type' => 'courses_type', 'posts_per_page' => get_post_meta($post->ID, true), 'orderby' => 'title', 'order' => 'asc', 'posts_per_page' => -1, 'meta_query' =>
							array(
								array(
									'key' => 'language',
									'value' => 'spanish',
								),
								array(
									'key' => 'program',
									'value' => 'graduate',
								)
							)
						 ) ); ?>
						 
					<h2 id="spanish-courses">Spanish Courses (<?php echo $qt; ?>)</h2>

					<?php if ( $spanish_courses_loop->have_posts() ) : while ( $spanish_courses_loop->have_posts() ) : $spanish_courses_loop->the_post(); ?>
					
					<h3><?php the_title(); ?></h3>
					<span class="instructors">
						<strong>Instructor: </strong>
						<?php $instructor = get_field('instructor'); ?>
						
						<? if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						<?php $spanish_courses_loop->reset_postdata(); ?>
						<?php endif; ?>
					</span>
					<span class="instructors">
						<?php if(get_field('additional_instructors')) { ?>
						<strong>Additional: </strong><?php the_field('additional_instructors'); ?>
						<?php } ?>
					</span>
					<?php the_content(); ?>
					
					<?php endwhile; else : ?>
						<p>There are no undergraduate Spanish courses this quarter.</p>
					<?php endif; ?>
					
					<?php // Portuguese Courses ?>
					<?php $portuguese_courses_loop = new WP_Query( 
						array( 'courses_cat' => "'$qt'", 'post_type' => 'courses_type', 'posts_per_page' => get_post_meta($post->ID, true), 'orderby' => 'title', 'order' => 'asc', 'posts_per_page' => -1, 'meta_query' =>
							array(
								array(
									'key' => 'language',
									'value' => 'portuguese',
								),
								array(
									'key' => 'program',
									'value' => 'graduate',
								)
							)
						) ); ?>
						 
					<h2 id="portuguese-courses">Portuguese Courses (<?php echo $qt; ?>)</h2>

					<?php if ( $portuguese_courses_loop->have_posts() ) : while ( $portuguese_courses_loop->have_posts() ) : $portuguese_courses_loop->the_post(); ?>
								
					<h3><?php the_title(); ?></h3>
					<span class="instructors">
						<strong>Instructor: </strong>
						<?php $instructor = get_field('instructor'); ?>
						
						<? if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						<?php $portuguese_courses_loop->reset_postdata(); ?>
						<?php endif; ?>
					</span>
					<span class="instructors">
						<?php if(get_field('additional_instructors')) { ?>
						<strong>Additional: </strong><?php the_field('additional_instructors'); ?>
						<?php } ?>
					</span>
					<?php the_content(); ?>
					
					<?php endwhile; else : ?>
						<p>There are no undergraduate Portuguese courses this quarter.</p>
					<?php endif; ?>
					
					<?php // Other Courses ?>
					<?php $other_courses_loop = new WP_Query( 
						array( 'courses_cat' => "'$qt'", 'post_type' => 'courses_type', 'posts_per_page' => get_post_meta($post->ID, true), 'orderby' => 'title', 'order' => 'asc', 'posts_per_page' => -1, 'meta_query' =>
							array(
								array(
									'key' => 'language',
									'value' => 'other',
								),
								array(
									'key' => 'program',
									'value' => 'graduate',
								)
							)
						) ); ?>
						 
					<h2 id="other-courses">Other Courses (<?php echo $qt; ?>)</h2>

					<?php if ( $other_courses_loop->have_posts() ) : while ( $other_courses_loop->have_posts() ) : $other_courses_loop->the_post(); ?>
								
					<h3><?php the_title(); ?></h3>
					<span class="instructors">
						<strong>Instructor: </strong>
						<?php $instructor = get_field('instructor'); ?>
						
						<? if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						<?php $other_courses_loop->reset_postdata(); ?>
						<?php endif; ?>
					</span>
					<span class="instructors">
						<?php if(get_field('additional_instructors')) { ?>
						<strong>Additional: </strong><?php the_field('additional_instructors'); ?>
						<?php } ?>
					</span>
					<?php the_content(); ?>
					
					<?php endwhile; else : ?>
						<p>There are no graduate courses for other languages this quarter.</p>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
			<?php endwhile; else : ?>
			<?php endif; ?>

<?php get_footer(); ?>
