<?php get_header(); ?>

			<div class="content main">
			
				<header>
					<h1>Faculty</h1>
					<p><?php echo category_description(); ?></p>
					<div class="filter">
						<div class="fields button-group" data-filter-group="field">
							<ul>
								<button class="button btn all is-checked" data-filter="">View All</button>
								<button class="button btn officers" data-filter=".officers">Department Officers</button>
							</ul>
							<h4>Field of Study</h4>
							<ul>
								<button class="button btn ealinguistics" data-filter=".ealinguistics">Asian Linguistics</button>
								<button class="button btn religion" data-filter=".religion">Asian Religions</button>
								<button class="button btn chinese" data-filter=".chinese">Chinese</button>
								<button class="button btn japanese" data-filter=".japanese">Japanese</button>
								<button class="button btn korean" data-filter=".korean">Korean</button>
								<button class="button btn sa" data-filter=".sa">South Asia</button>
								<button class="button btn ssea" data-filter=".ssea">Southeast Asia</button>
							</ul>
						</div>
					</div>
					<h2 class="filter-title">All Faculty</h2>
				</header>			

				<div class="people-list">

					<ul <?php post_class('cf'); ?>>
					
					<?php 						
						$core_loop = new WP_Query( array( 'people_cat' => 'faculty', 'post_type' => 'people_type', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC'));						
					?>
					
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
										
						<a href="<?php the_permalink() ?>" class="person-item <?php if(get_field('field')) { echo implode(' ', get_field('field')); } ?> <?php the_field('person_type'); ?>">
							<li><?php if(get_field('photo')) {
								$image = get_field('photo');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'bones-thumb-100';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo" />
								<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" width="100px" height="100px" class="photo" />
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php if(get_field('interest')) { ?>
									<dd class="interest">
										<?php the_field('interest'); ?>
									</dd>
									<?php } ?>
								</dl>
							</li>
						</a>

					<?php endwhile; ?>
					
					</ul>

				</div>

			</div>

<?php get_footer(); ?>
