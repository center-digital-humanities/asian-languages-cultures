<?php get_header(); ?>
			<div class="content main">
			
				<div class="col">
					<?php $research_loop = new WP_Query( array( 'post_type' => 'research_type', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'asc' ) ); ?>
					<section>
						<?php while ( $research_loop->have_posts() ) : $research_loop->the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">	
							<h2 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
	
							<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail( 'bones-thumb-640' ); ?></a>
							<span class="researcher">
							<?php $post_objects = get_field('researcher');
							if( $post_objects ): ?>
								<strong>Researcher: </strong>
							    <?php foreach( $post_objects as $post_object ): ?>
									<a href="<?php echo get_permalink($post_object->ID); ?>"><?php echo get_the_title($post_object->ID); ?></a>
							    <?php endforeach; ?>
							<?php endif; ?>
							</span>
							<p><?php
							$content = get_the_content();
							$trimmed_content = wp_trim_words( $content, 50, '...' );
							echo $trimmed_content;
							?></p>
							<a href="<?php the_permalink() ?>" class="btn">Read More</a>
			
						</article>
						<?php endwhile; ?>
					</section>							
				</div>

				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>
