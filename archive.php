<?php get_header(); ?>
			<div class="content">
				<div class="col" id="main-content" role="main">
					<?php if (is_archive() || is_category()) { ?>
					<h1 class="page-title">
						<?php single_cat_title(); ?>
					</h1>
					<?php } elseif (is_tag()) { ?>
					<h1 class="page-title">
						<span><?php _e( 'Articles Tagged:', 'bonestheme' ); ?></span> <?php single_tag_title(); ?>
					</h1>
					<?php } elseif (is_author()) {
						global $post;
						$author_id = $post->post_author;
					?>
					<h1 class="page-title">
						<span><?php _e( 'Articles By:', 'bonestheme' ); ?></span> <?php the_author_meta('display_name', $author_id); ?>
					</h1>
					<?php } elseif (is_day()) { ?>
					<h1 class="page-title">
						<span><?php _e( 'Daily Archives:', 'bonestheme' ); ?></span> <?php the_time('l, F j, Y'); ?>
					</h1>
					<?php } elseif (is_month()) { ?>
					<h1 class="page-title">
						<span><?php _e( 'Monthly Archives:', 'bonestheme' ); ?></span> <?php the_time('F Y'); ?>
					</h1>
					<?php } elseif (is_year()) { ?>
					<h1 class="page-title">
						<span><?php _e( 'Yearly Archives:', 'bonestheme' ); ?></span> <?php the_time('Y'); ?>
					</h1>
					<?php } ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
						<h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
						<div class="post-details">
							<span class="publish-date"><strong>Published:</strong> <?php echo get_the_date(); ?></span><?php the_tags( '<span class="tags"><strong>Tags:</strong> ', ', ', '</span>' ); ?>
						</div>
						<section class="entry-content cf">
							
						<?php if (is_category('undergraduate-announcements')) {
							the_post_thumbnail( 'bones-thumb-340' );
						}  else {
							the_post_thumbnail( 'bones-thumb-640' );
						} ?>
							<?php the_excerpt(); ?>
							<a href="<?php the_permalink() ?>" class="btn">Read More</a>
						</section>
					</article>

					<?php endwhile; ?>
					
					<?php bones_page_navi(); ?>

					<?php else : ?>
					
					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<section>
							<p>There is nothing available to show here at this time. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>