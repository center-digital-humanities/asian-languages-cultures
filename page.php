<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						<h1 class="page-title"><?php the_title(); ?></h1>

						<section>
							<?php if ( is_page( 'study-abroad' )  ) : ?>
							<script type="text/javascript">
								jQuery("document").ready(function($) {
									$(document).ready(function(){
									  $('#bxslider').bxSlider({
									  	autoHover: true,
									  	auto: false,
									  });
									});
								});
							</script>
							<div id="slider">
								<ul id="bxslider">
									<?php $slides = new WP_Query( array( 'post_type' => 'slider_type', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'asc', 'meta_query' => array( array( 'key' => 'page', 'value' => 'studyabroad')))); while ( $slides->have_posts() ) : $slides->the_post(); 
										$thumb_id = get_post_thumbnail_id();
										$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'bones-thumb-340', true);
										$thumb_url = $thumb_url_array[0];
									?>
										<li style="background-image: url('<?php echo($thumb_url) ?>');">
										</li>
									<?php endwhile; ?>
								</ul>
							</div>
							<?php endif; ?>
							<?php wp_reset_postdata() ;?>
							<?php the_content(); ?>
						</section>
						<?php if ( is_page( 'alc-friends' )  ) : ?>
						<aside>
							<a href="http://www.humanities.ucla.edu/calendar/month.calendar/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/alumni_humanitiesCalBt.jpg" alt="alumni_humanitiesCalBt" /></a>
							<a href="http://happenings.ucla.edu/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/alumni_happeningsBt.jpg" alt="alumni_happeningsBt" /></a>
							<div class="imgLink_lrg"><a href="https://alumni.ucla.edu/_updates/index.cfm?school=ALC" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/17-0872_250.jpg" alt="17-0872 250" />Alumni Online Update</a></div>
							<div class="imgLink_lrg"><a href="https://www.bol.ucla.edu/cgi-ssl/accounts/setforward" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/7708852_alumniNet.jpg" alt="7708852 alumniNet" />UCLA Email Account</a></div>
							<div class="imgLink_lrg"><a href="http://alc.ucla.edu/alumni-and-friends/alumni-network"><img src="<?php echo get_template_directory_uri(); ?>/library/images/Bruin-Day-2012-964.jpg" alt="Bruin-Day-2012-964" />Bruin Works / Alumni Networking</a></div>
						</aside>
						<div class="friends-footer">
							<div class="imgLink"><a href="https://giving.ucla.edu/alc" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/alumni_giving.jpg" alt="" />Giving</a></div>
							<div class="imgLink"><a href="http://www.youtube.com/watch?v=PT_o3aKhXaw&amp;feature=player_embedded"><img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla_optimists_165.jpg" alt="ucla optimists 165" />Optimists</a></div>
							<div class="imgLink"><a href="http://alc.ucla.edu/alumni-and-friends/chancellor-s-society"><img src="<?php echo get_template_directory_uri(); ?>/library/images/20074435_chancellor.jpg" alt="20074435 chancellor" />Chancellor's Society</a></div>
							<div class="imgLink"><a href="http://alc.ucla.edu/alumni-and-friends/keep-in-touch-with-us"><img src="<?php echo get_template_directory_uri(); ?>/library/images/6123208_giving.jpg" alt="6123208 giving" />Keep In Touch With Us</a></div>
						</div>
						<?php endif; ?>
					</article>

					<?php endwhile; else : ?>

					<article id="post-not-found" class="hentry cf">
							<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						<section class="entry-content">
							<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
					</article>

					<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>
