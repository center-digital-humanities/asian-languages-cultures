<?php get_header(); ?>

			<div class="content main">
			
				<header>
					<h1><?php single_cat_title(); ?></h1>
					<?php $category_description = category_description();
					if ( ! empty( $category_description ) )
					echo apply_filters( 'category_archive_meta', '<p>' . $category_description . '</p>' );
					?>
					<!--<input type="search" class="search-filter" placeholder="Type a name..." />-->	
					<div class="filter">
						<div class="fields button-group" data-filter-group="field">
							<h4>Field of Study</h4>
							<ul>
								<button class="button btn all is-checked" data-filter="">View All</button>
								<button class="button btn chinese" data-filter=".chinese">Chinese</button>
								<button class="button btn japanese" data-filter=".japanese">Japanese</button>
								<button class="button btn korean" data-filter=".korean">Korean</button>
								<button class="button btn ealinguistics" data-filter=".ealinguistics">E.A. Linguistics</button>
								<button class="button btn religion" data-filter=".religion">Buddhism</button>
							</ul>
						</div>
					</div>
					<h2 class="filter-title">All Graduates</h2>
				</header>

				<div class="people-list">

					<ul <?php post_class('cf'); ?>>
					
					<?php 
						$people_loop = new WP_Query( array( 'people_cat' => 'grad', 'post_type' => 'people_type', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC'));
					?>
					
					<?php while ( $people_loop->have_posts() ) : $people_loop->the_post(); ?>
										
						<li class="person-item <?php if(get_field('field')) { echo implode(' ', get_field('field')); } ?>">
							<?php if(get_field('photo')) {
							$image = get_field('photo');
							if( !empty($image) ): 
								// vars
								$url = $image['url'];
								$title = $image['title'];
								// thumbnail
								$size = 'bones-thumb-100';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ];
							endif; ?>
							<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo" />
							<?php } ?>
							<dl>
								<dt class="name"><?php the_title(); ?></dt>
								<?php if(get_field('interest')) { ?>
								<dd class="interest">
									<strong><?php the_field('interest'); ?></strong>
								</dd>
								<?php } ?>
								<?php if(get_field('email_address')) { ?>
								<dd class="email">
									<a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a>
								</dd>
								<?php } ?>
								<dd class="bio">
									<?php the_content(); ?>
								</dd>
							</dl>
						</li>
	
					<?php endwhile; ?>
					</ul>

				</div>

			</div>

<?php get_footer(); ?>